\babel@toc {english}{}
\contentsline {chapter}{Contents}{2}{chapter*.2}% 
\contentsline {chapter}{List of Figures}{10}{chapter*.3}% 
\contentsline {chapter}{List of Tables}{11}{chapter*.4}% 
\contentsline {chapter}{Abbreviations}{12}{table.6}% 
\contentsline {chapter}{Abstract}{14}{section*.7}% 
\contentsline {chapter}{Declaration of Authorship}{15}{section*.8}% 
\contentsline {chapter}{Copyright Statement}{16}{section*.9}% 
\contentsline {chapter}{Acknowledgements}{17}{section*.14}% 
\contentsline {chapter}{\numberline {1}Introduction}{18}{chapter.15}% 
\contentsline {section}{\numberline {1.1}Overview of this work}{18}{section.16}% 
\contentsline {section}{\numberline {1.2}Introduction to pulsars}{19}{section.17}% 
\contentsline {subsection}{\numberline {1.2.1}Discovery, population, and properties}{20}{subsection.18}% 
\contentsline {subsection}{\numberline {1.2.2}Theoretical background}{24}{subsection.25}% 
\contentsline {subsubsection}{Energy loss, rotation and magnetism}{24}{section*.26}% 
\contentsline {subsubsection}{Emission}{26}{section*.34}% 
\contentsline {subsubsection}{Propagation and observational quantities}{27}{section*.35}% 
\contentsline {paragraph}{Frequency dispersion}{27}{section*.36}% 
\contentsline {subsection}{\numberline {1.2.3}Timing}{28}{subsection.41}% 
\contentsline {subsection}{\numberline {1.2.4}Pulsar wind}{28}{subsection.43}% 
\contentsline {section}{\numberline {1.3}Binary pulsars}{29}{section.45}% 
\contentsline {subsection}{\numberline {1.3.1}Introduction}{29}{subsection.46}% 
\contentsline {subsection}{\numberline {1.3.2}Timing in binaries}{30}{subsection.47}% 
\contentsline {subsubsection}{Keplerian delays}{30}{section*.48}% 
\contentsline {subsubsection}{The mass function}{30}{section*.49}% 
\contentsline {subsection}{\numberline {1.3.3}Formation}{32}{subsection.56}% 
\contentsline {subsubsection}{Recycling}{32}{section*.57}% 
\contentsline {subsubsection}{Angular momentum transfer}{33}{section*.59}% 
\contentsline {subsubsection}{X-Ray binaries and their evolution}{34}{section*.60}% 
\contentsline {section}{\numberline {1.4}Spider pulsars}{37}{section.63}% 
\contentsline {subsection}{\numberline {1.4.1}Classification and formation}{37}{subsection.64}% 
\contentsline {subsection}{\numberline {1.4.2}Physical properties}{39}{subsection.67}% 
\contentsline {subsubsection}{Irradiation and ablation}{39}{section*.68}% 
\contentsline {subsubsection}{Eclipses}{40}{section*.70}% 
\contentsline {subsubsection}{Optical properties}{41}{section*.72}% 
\contentsline {subsubsection}{Magnetic fields and binary properties}{43}{section*.75}% 
\contentsline {section}{\numberline {1.5}Transitional millisecond pulsars}{43}{section.76}% 
\contentsline {subsection}{\numberline {1.5.1}Accretion and the transition}{43}{subsection.77}% 
\contentsline {subsection}{\numberline {1.5.2}Known tMSPs}{44}{subsection.78}% 
\contentsline {subsection}{\numberline {1.5.3}tMSP characteristics}{45}{subsection.79}% 
\contentsline {section}{\numberline {1.6}Statistical, numerical, and machine learning methods}{46}{section.80}% 
\contentsline {subsection}{\numberline {1.6.1}Bayesian inference and sampling methods}{46}{subsection.81}% 
\contentsline {subsection}{\numberline {1.6.2}Icarus}{48}{subsection.88}% 
\contentsline {subsection}{\numberline {1.6.3}Machine learning}{50}{subsection.91}% 
\contentsline {subsubsection}{The Random Forest classifier}{50}{section*.92}% 
\contentsline {subsubsection}{Performance metrics}{51}{section*.94}% 
\contentsline {chapter}{\numberline {2}Instrumentation, observations, and simulations}{54}{chapter.101}% 
\contentsline {section}{\numberline {2.1}Introduction}{54}{section.102}% 
\contentsline {section}{\numberline {2.2}Instrumentation and data analysis}{55}{section.103}% 
\contentsline {subsection}{\numberline {2.2.1}Instrumentation}{55}{subsection.104}% 
\contentsline {subsubsection}{ULTRACAM}{55}{section*.105}% 
\contentsline {subsubsection}{HiPERCAM}{57}{section*.107}% 
\contentsline {subsubsection}{Telescopes}{57}{section*.108}% 
\contentsline {subsection}{\numberline {2.2.2}Data reduction}{58}{subsection.109}% 
\contentsline {subsubsection}{The ULTRACAM pipeline}{58}{section*.110}% 
\contentsline {subsection}{\numberline {2.2.3}Calibration of photometric light curves}{61}{subsection.115}% 
\contentsline {subsubsection}{PyCam}{61}{section*.116}% 
\contentsline {paragraph}{Reduction, timing, and flux calibration}{62}{section*.117}% 
\contentsline {paragraph}{Uncertainties}{63}{section*.120}% 
\contentsline {subsubsection}{Colour terms}{65}{section*.127}% 
\contentsline {section}{\numberline {2.3}Observational work}{65}{section.128}% 
\contentsline {subsection}{\numberline {2.3.1}Scheduler}{65}{subsection.129}% 
\contentsline {subsection}{\numberline {2.3.2}ULTRACAM on the NTT: October 2017}{69}{subsection.132}% 
\contentsline {subsubsection}{Observing with ULTRACAM}{69}{section*.133}% 
\contentsline {subsubsection}{P101 targets}{69}{section*.134}% 
\contentsline {paragraph}{PSR J2241--5236}{69}{section*.135}% 
\contentsline {paragraph}{3FGL J2017--16}{69}{section*.136}% 
\contentsline {paragraph}{3FGL J0427.9--6704}{69}{section*.137}% 
\contentsline {paragraph}{3FGL J0744.1--2523}{70}{section*.139}% 
\contentsline {paragraph}{PSR J2339--0533}{71}{section*.141}% 
\contentsline {subsection}{\numberline {2.3.3}Other Sources}{72}{subsection.142}% 
\contentsline {subsubsection}{PSR J1628--3205}{72}{section*.144}% 
\contentsline {subsubsection}{3FGL J0427.9--6704}{74}{section*.146}% 
\contentsline {subsubsection}{PSR J1306--40}{76}{section*.148}% 
\contentsline {section}{\numberline {2.4}Simulations}{77}{section.150}% 
\contentsline {subsection}{\numberline {2.4.1}Signal-to-noise ratio}{77}{subsection.151}% 
\contentsline {subsection}{\numberline {2.4.2}Light curve simulations}{77}{subsection.152}% 
\contentsline {subsubsection}{The simulation}{78}{section*.153}% 
\contentsline {subsubsection}{Recovery of parameters}{80}{section*.162}% 
\contentsline {section}{\numberline {2.5}Conclusion}{84}{section.165}% 
\contentsline {chapter}{\numberline {3}Optical photometry of two millisecond pulsars}{85}{chapter.166}% 
\contentsline {section}{\numberline {3.1}Introduction}{85}{section.167}% 
\contentsline {section}{\numberline {3.2}Optical observations}{87}{section.168}% 
\contentsline {subsection}{\numberline {3.2.1}ULTRACAM on the NTT}{87}{subsection.169}% 
\contentsline {subsection}{\numberline {3.2.2}Observations of J1227}{88}{subsection.173}% 
\contentsline {subsection}{\numberline {3.2.3}Observations of J1023}{91}{subsection.174}% 
\contentsline {subsection}{\numberline {3.2.4}Radial Velocities}{92}{subsection.175}% 
\contentsline {section}{\numberline {3.3}Asymmetries}{92}{section.176}% 
\contentsline {section}{\numberline {3.4}Numerical modelling}{95}{section.179}% 
\contentsline {subsection}{\numberline {3.4.1}Icarus}{95}{subsection.180}% 
\contentsline {subsection}{\numberline {3.4.2}Standard symmetrical direct heating model}{97}{subsection.182}% 
\contentsline {subsection}{\numberline {3.4.3}Single-spot heating model}{100}{subsection.186}% 
\contentsline {subsection}{\numberline {3.4.4}Heat redistribution}{102}{subsection.188}% 
\contentsline {section}{\numberline {3.5}Results}{103}{section.193}% 
\contentsline {subsection}{\numberline {3.5.1}J1227}{103}{subsection.194}% 
\contentsline {subsubsection}{Standard model}{103}{section*.195}% 
\contentsline {subsubsection}{Hot spot model}{106}{section*.196}% 
\contentsline {subsubsection}{Cold Spots}{106}{section*.198}% 
\contentsline {subsubsection}{Heat redistribution}{108}{section*.199}% 
\contentsline {subsubsection}{Modelling assuming a filled Roche lobe}{108}{section*.200}% 
\contentsline {subsubsection}{Modelling with fixed inclination}{108}{section*.201}% 
\contentsline {subsection}{\numberline {3.5.2}J1023}{111}{subsection.205}% 
\contentsline {subsubsection}{Standard model}{111}{section*.206}% 
\contentsline {subsubsection}{Hot spot model}{113}{section*.208}% 
\contentsline {subsubsection}{Heat redistribution}{113}{section*.209}% 
\contentsline {section}{\numberline {3.6}Discussion}{115}{section.210}% 
\contentsline {subsection}{\numberline {3.6.1}Filling factor}{115}{subsection.211}% 
\contentsline {subsection}{\numberline {3.6.2}Asymmetries}{118}{subsection.214}% 
\contentsline {subsection}{\numberline {3.6.3}J1227}{119}{subsection.216}% 
\contentsline {subsection}{\numberline {3.6.4}J1023}{120}{subsection.218}% 
\contentsline {section}{\numberline {3.7}Conclusions}{123}{section.221}% 
\contentsline {chapter}{\numberline {4}Periodicity search and classification pipeline for GOTO}{125}{chapter.222}% 
\contentsline {section}{\numberline {4.1}Introduction}{125}{section.223}% 
\contentsline {subsection}{\numberline {4.1.1}Background}{125}{subsection.224}% 
\contentsline {subsection}{\numberline {4.1.2}The GOTO instrument}{126}{subsection.225}% 
\contentsline {subsection}{\numberline {4.1.3}Summary of the project}{128}{subsection.226}% 
\contentsline {subsection}{\numberline {4.1.4}Introduction to star types}{128}{subsection.227}% 
\contentsline {paragraph}{Semi-regular variable stars (SR)}{128}{section*.230}% 
\contentsline {paragraph}{Mira Variables (M)}{130}{section*.231}% 
\contentsline {paragraph}{Detached Algol binaries (EA)}{130}{section*.232}% 
\contentsline {paragraph}{$\boldsymbol {\beta }$ Lyrae binaries (EB)}{130}{section*.233}% 
\contentsline {paragraph}{W Ursae Majoris binaries (EW)}{131}{section*.234}% 
\contentsline {paragraph}{RR Lyrae variables (RRC and RRAB)}{131}{section*.235}% 
\contentsline {paragraph}{Spotted variables with rotation modulation (ROT)}{131}{section*.236}% 
\contentsline {section}{\numberline {4.2}Data Acquisition}{131}{section.237}% 
\contentsline {subsection}{\numberline {4.2.1}Simulation of GOTO data}{131}{subsection.238}% 
\contentsline {subsection}{\numberline {4.2.2}Obtaining and reducing photometry}{134}{subsection.241}% 
\contentsline {section}{\numberline {4.3}Periodicity Searching}{137}{section.244}% 
\contentsline {subsection}{\numberline {4.3.1}First-pass elimination of non-varying sources}{137}{subsection.245}% 
\contentsline {subsection}{\numberline {4.3.2}Identifying periodic sources}{139}{subsection.248}% 
\contentsline {subsubsection}{Algorithm choices}{139}{section*.249}% 
\contentsline {paragraph}{Generalised Lomb-Scargle periodogram}{140}{section*.250}% 
\contentsline {paragraph}{Multi-harmonic analysis of variance (MHAOV)}{142}{section*.255}% 
\contentsline {paragraph}{Initial performance}{145}{section*.258}% 
\contentsline {subsubsection}{Periodogram peak significance}{146}{section*.259}% 
\contentsline {subsection}{\numberline {4.3.3}Algorithm performance}{147}{subsection.264}% 
\contentsline {subsubsection}{Aliasing and jitter}{148}{section*.265}% 
\contentsline {subsubsection}{Testing data}{151}{section*.270}% 
\contentsline {subsubsection}{Factors influencing period recovery}{151}{section*.272}% 
\contentsline {paragraph}{Light curve shape}{151}{section*.273}% 
\contentsline {section}{\numberline {4.4}Classification of periodic sources}{154}{section.277}% 
\contentsline {subsection}{\numberline {4.4.1}Light curve modelling}{156}{subsection.279}% 
\contentsline {subsection}{\numberline {4.4.2}The Random Forest classifier}{158}{subsection.289}% 
\contentsline {subsubsection}{Training and testing datasets}{158}{section*.290}% 
\contentsline {paragraph}{ASAS-SN training data and classifier features}{158}{section*.291}% 
\contentsline {paragraph}{GOTO testing data and source matching}{159}{section*.293}% 
\contentsline {subsection}{\numberline {4.4.3}Classifier analysis}{162}{subsection.295}% 
\contentsline {subsubsection}{Feature importance}{162}{section*.296}% 
\contentsline {subsubsection}{Classifier performance}{163}{section*.299}% 
\contentsline {paragraph}{Semi-regular variables}{165}{section*.302}% 
\contentsline {paragraph}{W Ursae Majoris-type binaries}{168}{section*.305}% 
\contentsline {paragraph}{Spotted variables}{169}{section*.307}% 
\contentsline {subsubsection}{Absolute magnitude}{169}{section*.308}% 
\contentsline {subsubsection}{Removal of light curves}{170}{section*.310}% 
\contentsline {subsubsection}{Classification of ASAS-SN light curves}{172}{section*.312}% 
\contentsline {chapter}{\numberline {5}Discussion and conclusions}{176}{chapter.318}% 
\contentsline {section}{\numberline {5.1}Discussion}{176}{section.319}% 
\contentsline {subsection}{\numberline {5.1.1}tMSPs}{176}{subsection.320}% 
\contentsline {subsubsection}{Lessons for modelling}{176}{section*.321}% 
\contentsline {subsubsection}{Intricacies of Icarus}{176}{section*.322}% 
\contentsline {paragraph}{Distance and extinction}{176}{section*.323}% 
\contentsline {paragraph}{Temperatures}{177}{section*.324}% 
\contentsline {paragraph}{Spot constraints}{178}{section*.325}% 
\contentsline {subsection}{\numberline {5.1.2}GOTO}{179}{subsection.326}% 
\contentsline {subsubsection}{Lessons for surveys}{179}{section*.327}% 
\contentsline {subsubsection}{The importance of ancillary information}{180}{section*.328}% 
\contentsline {subsubsection}{Other surveys}{181}{section*.329}% 
\contentsline {section}{\numberline {5.2}Conclusions}{183}{section.330}% 
