
\paragraph{Gaussian Process Modelling} \label{sec:gpm}
If we assume that the behaviour of a function depends entirely on the covariance matrix, it is possible to predict the value of that function at some point using only the knowledge of its value at other points. That is, we can extrapolate some data set by inverting the probability statement in equation \ref{eq:probability_statement} to find the value of the function at a new position, $x_*$, based on existing measurements, $y(x)$. To do this it is necessary to either know the form of the covariance matrix (or be able to calculate it from the data set), or to have enough data points to construct a training data set with enough fixed points. 
%If we have this, then for a set of $n$ training data points, $\mathbf{x}$, and $n_*$ test data points, $\mathbf{x}_*$, we can invert equation equation \ref{eq:probability_statement} to obtain the equations
%\begin{alignat}{1}
%\mathbf{m}_* &= K(\mathbf{x}_*, \mathbf{x})^TK(\mathbf{x},\mathbf{x})^{-1} \mathbf{y}\\
%\mathbf{C}_* &= K(\mathbf{x}_*, \mathbf{x}_*) - K(\mathbf{x}_*, \mathbf{x})^T K(\mathbf{x}, \mathbf{x})^{-1} K(\mathbf{x}_*, \mathbf{x}),
%\end{alignat}
%or, if the original measurements have non-zero mean, $\mathbf{\mu}$, 
%\begin{alignat}{1}
%\mathbf{m}_* &= \mathbf{\mu}_* + K(\mathbf{x}_*, \mathbf{x})^TK(\mathbf{x},\mathbf{x})^{-1} \mathbf{y} - \mathbf{\mu}\\
%\mathbf{C}_* &= K(\mathbf{x}_*, \mathbf{x}_*) - K(\mathbf{x}_*, \mathbf{x})^T K(\mathbf{x}, \mathbf{x})^{-1} K(\mathbf{x}_*, \mathbf{x}).
%\end{alignat}
%In this notation, $\mathbf{m}_*$ is the posterior mean, $\mathbf{C}_*$ is the posterior variance, $K(x, x)$ is an $n\times n$ covariance matrix, $K(x_*, x)$ is an $n_*\times n$ covariance matrix, $\mathbf{y} = y(\mathbf{x})$ are the training outputs and $\mathbf{\mu}$ is the mean of the training data. 
Here I have followed \cite{GPM}, in which a full treatment can be found. If the posterior mean and variance can be calculated from the covariance matrix of some training data set drawn from a Gaussian process, then they can be used to predict the initial distribution of that data set. An example of this can be seen in figure \ref{fig:gpm-model}. \\

\begin{figure}[h]
	\centering
	\includegraphics[width=0.8\textwidth]{./LitFigs/model.pdf}
	\caption{\textit{Left:} Training data points in blue and random function from which they were drawn (dashed line). This random function was produced by a Gaussian process some covariance matrix, $K$. \textit{Right: } Predicted posterior mean in solid blue,  standard deviation as a grey shaded region, with the original distribution in dashed orange. Where the data are not well-sampled we can see the predictive power of GPM.}
	\label{fig:gpm-model}
\end{figure}

For a more complicated data set, we may not be able to easily determine how the covariance matrix is populated. For the previous example, the data set was drawn from a Gaussian process with a covariance matrix containing just one kernel, but real data sets may have covariance matrices defined by a number of kernels. It is possible for a data set to be defined by some spectrum of kernels. When modelling these data sets, the difficulty lies in selecting which Gaussian kernels to use, as optimising their hyperparameters is typically a trivial optimisation problem. A canonical example of the use of GPM is in predicting the future trends of atmospheric CO2 concentration, based on a large data set. Shown in figure \ref{fig:co2}, we can use this dataset to extrapolate into the future by identifying the Gaussian kernels which will form the covariance function to accurately model the data. This example and the choice of kernels also follows \cite{GPM}, and I have chosen it since it includes the most commonly used kernels, which are as follows:
\begin{enumerate}
	\item To model the overall rising trend, we use a \textit{squared-exponential} kernel,
	\begin{equation}
	\label{eq:gpmse}
	k_1(x,x') = \theta_1 \exp \left( -\f{(x - x')^2}{2\theta_2^2} \right),
	\end{equation}
	where the two hyperparameters, $\theta_1$ and $\theta_2$ control the amplitude and characteristic length scale respectively.  
	\item To model the periodic variation, we use a periodic kernel with some decay away from periodicity, as we are not sure that the data is truly periodic over long scales. The periodic kernel (right) is combined with a squared-exponential kernel (left),
	\begin{equation}
	\label{}
	k_2(x, x') = \theta_3^2 \exp\left( - \f{(x - x')^2}{2\theta_4^2} \right) \cdot \exp\left( - \f{2}{\theta_5^2} \sin^2\left( \f{\pi \left( x - x'\right)}{P} \right) \right),
	\end{equation}
	where $\theta_3$ is the magnitude of the periodic component, $\theta_4$ is the decay time, $\theta_5$ is the smoothness, and $P$ is the period;
	\item To model the small departures from the general trend, we can use a \textit{rational quadratic} term,
	\begin{equation}
	\label{}
	k_3(x, x') = \theta_6^2 \left( 1 + \f{\left( x - x'\right)^2}{2\theta_8\theta_7^2} ^{-\theta_8}\right),
	\end{equation}
	where $\theta_6$ is the magnitude, $\theta_7$ is the typical length scale, and $\theta_8$ is the so-called shape parameter,  which quantifies the diffuseness of the length scales. This kernel works well for these data as it can accommodate different length scales.
	\item Finally, we define a noise kernel, with two terms. A squared exponential (correlated) term and a white (uncorrelated) term,
	\begin{equation}
	\label{}
	k_4(x_p, x_q) = \theta_9^2 \exp \left( - \f{\left( x_p - x_q\right)^2}{2\theta_{10}^2} \right) + \theta_{11}\delta_{pq},
	\end{equation}
	where $\theta_9$ is the magnitude of the correlated noise component, $\theta_{10}$ is the length scale, $\theta_{11}$ is the magnitude of the uncorrelated noise component, and $\delta_{pq}$ is the Kronecker delta. The correlated noise and first kernel, $k_1$ are of the same form. When optimising, one will become dominant on long length scales, and the other on small. We class small-scale trend as noise in this example, but for a different situation this could certainly be the other way around.
\end{enumerate}

So, the kernel that we use to construct the covariance matrix will be a sum of these,
\begin{equation}
\label{}
k\left(x - x'\right) = k_1\left(x - x'\right) + k_2\left(x - x'\right) + k_3\left(x - x'\right) +k_4\left(x - x'\right),
\end{equation}
with hyperparameters $\mathbf{\theta} = (\theta_1,\dots,\theta_{11})^{T}$. The hyperparameters are fit by optimising the marginal likelihood. In practice it is common to first run a gradient optimiser, then further optimise the hyperparameters using an MCMC sampler. Considering figure \ref{fig:co2}, one may note that the GPM prediction has not accurately predicted the permanent increase of atmospheric CO2 concentration above 400 ppm that occurred in 2016 . However, it is hard to say if that is due to a shortcoming of the model, or if the atmospheric CO2 concentration in increasing at an accelerating rate. An example of the use of GPM in the context of this work can be seen in section \ref{sec:J0427}.\\

\begin{figure}[h]
	\centering
	\includegraphics[width=0.9\textwidth]{./LitFigs/co2.png}
	\caption{In blue, measured CO2 concentrations at Mauna Loa. In grey, the $1\sigma$ margin of the GPM prediction. We can clearly recover the periodic oscillations and the overall trend. Of course, the extrapolation is best closest to the data set, but the same trends are continued far into the future, albeit with much larger uncertainties. Image credit: \cite{GPM}}
	\label{fig:co2}
\end{figure}


\paragraph{Covariance}
While the majority of noise is uncorrelated white noise, it is often important to consider co-variate (or multi-variate) Gaussian noise, where one data point affects the value of another. This is called covariance, and is formally defined as the joint probability of two random variables. The covariance of some dataset is described by the so-called covariance matrix, $K$. For a dataset containing $N$ data points the covariance matrix of size $N\times N$ will be
\begin{equation}
\label{}
K(x_i, x_j) = \left(\begin{matrix}
k(x_1, x_1) & k(x_1, x_2) & \cdots & k(x_1, x_N)\\
k(x_2, x_1) & k(x_2, x_2) & \cdots & k(x_2, x_N)\\
\vdots & \vdots & \vdots & \vdots\\
k(x_N, x_1) & k(x_N, x_2) & \cdots & k(x_N, x_N)\\
\end{matrix}\right),
\end{equation}
where $k(x_i, x_j)$ represents the covariance of data points $x_i$ and $x_j$. Uncorrelated (or independent) Gaussian noise has a diagonal covariance matrix, $K = K(x_i, x_i)$. The value of these diagonal elements is just the variance of the corresponding data point. If we have some relation, $y_i = y(x_i )$, then the y-value for each x-position will be drawn from a Normal distribution, $N$,
\begin{equation}
\label{eq:probability_statement}
p(y_i) = N(\mu_i, K_{ij}),
\end{equation}
where $\mu_i$ is the mean of the data point at position $x_i$. By adding in off-diagonal terms to the covariance matrix, the $y$-value at some $x$ will affect the $y$-position at another.\\

%Uncorrelated (or independent) Gaussian noise has a diagonal covariance matrix, such that
%
%\begin{equation}
%\label{}
%K_{uncor}(x_i, x_j) = \left(\begin{matrix}
%k(x_1, x_1) & 0 & \cdots & 0\\
%0 & k(x_2, x_2) & \cdots & 0\\
%\vdots & \vdots & \vdots & \vdots\\
%0 & 0 & \cdots & k(x_N, x_N)\\
%\end{matrix}\right).
%\end{equation}
%
%The value of these diagonal elements is just the variance of the corresponding data point. For a data point with mean $\mu_1$, for example, at position $x_1$, the variance will be $k(x_1, x_1) = \sigma^2_1$. If we have some relation $y_i = y(x_i)$, then the $y$-value for each $x$-position will be drawn from a Normal distribution, $N$,
%
%\begin{equation}
%\label{eq:probability_statement}
%p(y_i) = N(\mu_i, K_{ij}),
%\end{equation}
%
%which reduces to
%
%\begin{equation}
%\label{}
%y_i = \mu_i + N(0, \sigma_i^2)
%\end{equation}
%
%for the diagonal (uncorrelated) covariance matrix. That is, the $y$ value is the mean $x$ value, plus some Gaussian noise with variance $\sigma_i^2$. By adding in off-diagonal terms to the covariance matrix, now the $y$-value at some $x$ will affect the $y$-position at another.\\


In practice, it is possible to both construct the covariance matrix from the covariances of some data set and to construct it `artificially'. In the first case, the covariance of two data points is calculated using the equation
\begin{equation}
\label{eq:data-covar}
K_{ij} = {\rm cov}(x_i, x_j) = \expt{(x_i-\mu_i)(x_j-\mu_j)} \equiv \expt{x_ix_j} - \mu_i\mu_j,
\end{equation}
where $\mu_i=\expt{x_i}$ and $\mu_j=\expt{x_j}$. In the latter case, the matrix is populated using kernels, functions that define the nature and extent of the covariance. A very commonly used kernel is the squared-exponential kernel, defined by the equation
\begin{equation}
\label{}
K(x_i, x_j) = h^2 \exp\left( \f{-(x_i - x_j)^2}{\lambda^2} \right) = h^2 \exp\left( \f{-\tau^2}{\lambda^2} \right)
\end{equation}
where $\tau = (x_i - x_j)$ (often written as $(x-x')$), $h$ is some normalisation representing the amplitude of the kernel, and $\lambda$ is the kernel width, representing the correlation scale of the kernel. $h$ and $\lambda$ are known as hyperparameters, since they have no direct physical representation. Drawing samples from this co-variate Gaussian distribution yields what is seen in figure \ref{fig:covariance}.

\begin{figure}[h]
	\centering
	\includegraphics[width=1.\textwidth]{./LitFigs/covar.pdf}
	\caption{\textit{Top:} Representation of the covariance matrix, showing values as a colour map; darker colours show smaller covariances, with mauve representing zero. \textit{Bottom:} Sets of random functions drawn from the corresponding Gaussian process. On the left, the random functions are a good approximation of uncorrelated, white noise, with a diagonal covariance matrix. On the right it is clear that the random functions now show covariance, and this is clear from the covariance matrix.  }
	\label{fig:covariance}
\end{figure}



























As such, our investigation is centred around the nature of the compact object, since there is evidence both for it being a neutron star and a white dwarf. Conversely, there is evidence to suggest this source may be a tMSP (transitional millisecond pulsar) in an accreting state. In addition to the ULTRACAM data introduced previously we analyse a further orbit of data, also in SDSS (u', g', i') bands and with a cadence of approximately 10 seconds. This is a considerably higher time resolution than the OGLE data from \citet{J0427strader}. This higher-time resolution photometry allows us to reveal constant flickering and flaring of the accretion disk, as shown in figure \ref{fig:J0427lc}. While analysis of the flares can provide information about the nature of the accretion disc, our aim is to to remove them in order to investigate the underlying light curve. If this source can be shown to exhibit the characteristic quasi-sinusoidal variation of a redback system, with irradiation and ellipsoidal components, then this will strongly suggest a neutron star primary and therefore that this system may be a tMSP.\\

We initially attempted to remove the flares by scaling and subtracting the relatively flat u'-band flux, $f(u\prime, t)$, before fitting a two-term sine-series. This is equivalent to fitting the function
\begin{equation}
\label{eq:0427-fit}
f(i\prime / g\prime,t) = {} C + S\times f(u\prime,t) + A_1 \sin(2\pi\,\nu_1 (t - t_0) + 2\pi\,\phi_1) +A_2 \sin(2\pi\,\nu_2 (t - t_0) + 2\pi\,\phi_2)
\end{equation}
to the i' ($f(i\prime, t)$) and g' ($f(g\prime, t)$) fluxes, where $C$ is the flux offset, $S$ is some scaling factor, and $t_0$ is the time of the first observation. $A_1$ and $A_2$ are the amplitudes of the irradiation and ellipsoidal components, $\nu_1$ and $\nu_2$ there frequencies, with $\nu_2 = 2\nu_1$, and $\phi_1$ and $\phi_2$ are some phase offset. Figure \ref{fig:J0427-fit} shows this function fit to the i'- and g'-band light curves.


\begin{figure}[h!]
	\centering
	\includegraphics[clip, trim=3cm 0.1cm 3cm 1.65cm, width=\textwidth]{./LitFigs/model_lc.pdf}
	\caption{The function in equation \ref{eq:0427-fit} fit to i'- and g'-band data from one night of observation, with eclipses removed. While the fit indeed shows the characteristic variation, there is clearly a large amount of scatter from the flares still present. As such, the quality of these fits is quite poor, most notably in the g'-band light curve. Image credit: \citet{Kennedy2018}}
	\label{fig:J0427-fit}
\end{figure}


The high uncertainty of fit evident in figure \ref{fig:J0427-fit} implies that a better method is needed to remove the flares. We ran a Lomb-Scargle periodicity search separately for each band and night of data, with the aim of uncovering some periodicity of the flaring mechanism. We found a significant detection in one night of i'-band data, shown in figure \ref{fig:0427-ls}, of a periodicity of $\sim 22$ minutes. However, this periodicity was not seen, or was seen at different frequencies, in other nights and bands of data. As such, it appears that this quasiperiodicity may be transient in nature. However, if there was some underlying periodic driving mechanism of the flares, we postulated that it may be possible to model it as a Gaussian process. If successful, we could use the model to subtract the flares from the data more efficiently than the current method.\\

Using the techniques outlined in section \ref{sec:gpm}, we constructed a covariance matrix using a combination of periodic and non-periodic kernels to model the flaring mechanism. We used one night of unscaled data, from the start of the observation until the eclipse, as our training data, and the rest of that night's observations as test data. In particular, we attempted to predict the location of flares in the eclipse, though not predicting the actual eclipse itself. Many combinations of kernels were tested, and the best results were obtained using the combination
\begin{equation}
\label{}
\text{Squared exponential } \times \text{ periodic } + \text{ Mat\'{e}rn.}
\end{equation}
However, neither this nor any combination appeared to be able to correctly model the flares. One of the more successful results is shown in figure \ref{fig:0427gpm}. The failure of GPM to predict far into the extrapolation suggests that the quasiperiodicity is only present on very short timescales. That is, that the autocorrelation scale of these data is only a few minutes. The physical interpretation of this is not clear, but it means that it is possible to use photometry with a cadence of up to $\sim$2-3 minutes and still resolve individual flares. The significance of the initial 22-minute period is also unclear, though 22-minute periodicities in variable sources tend to indicate a white dwarf primary.\\

And so, we return to the initial question: what is the compact object at the heart of this system? It is hoped that new data will shed some light on this system. As such, this source was the centre of the ESO P102 proposal discussed in section \ref{sec:proposal}, and upcoming data releases from XMM-Newton and GAIA will provide further X-ray and distance measurements respectively. \todo{CONTENT FROM PROPOSAL SECTION OR REMOVE REFERENCE}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{./LitFigs/i_band_LS.pdf}
	\caption{Light curve (\textit{top}) and corresponding Lomb-Scargle periodogram (\textit{bottom}) for one night of i'-band data. The 22-minute periodicity can clearly be seen in the power spectrum. Image credit: \cite{J0427-us}}
	\label{fig:0427-ls}
\end{figure}
\begin{figure}
	\centering
	\includegraphics[clip, trim=4cm 0.7cm 4cm 1.8cm, width=0.9\textwidth]{./LitFigs/expsin+matern32.pdf}
	\caption{One night of i'-band test data (blue dots) with GPM predicted posterior mean (red line) and standard deviation (grey shaded region). The extrapolation region begins around phase 1.9, and the quick decay of any structure can be seen. }
	\label{fig:0427gpm}
\end{figure}
