\chapter{Discussion and conclusions}

\section{Discussion}
\subsection{tMSPs}
\subsubsection{Lessons for modelling}
The photometry of PSR~J1227--4853 (J1227) and PSR~J1023+0038 was in both cases exceptionally high quality and precisely calibrated. 
Despite this, chapter \ref{ch:tmsps} has shown that the modelling process is far from a straight shot to well-constrained system parameters.
Instead, this work can give useful insights about the ultra-precise modelling required with such high quality data; especially important as data of this quality is becoming increasingly more common with the usage of instruments like HiPERCAM and ULTRACAM.
The asymmetric nature of the optical light curves can no longer be hidden under observational noise or sparse sampling.
In this section we will discuss the impact some of the small changes, such as fixing or constraining certain parameters, has on the model fit, as well as what the next steps for modelling these systems might be.


\subsubsection{Intricacies of Icarus}
\paragraph{Distance and extinction}
The importance of distance estimates in the modelling of spider binaries is clear.
Without a measure of the distance the mass cannot be easily constrained (see, e.g., \citet{Thorstensen2005}).
However, frequently overlooked is the extinction parameter, $A_V$, which incorporates the effect of interstellar reddening.
Since this affects the colour of the object, i.e. the relative flux of each band, it can have a pronounced effect on both the temperature and the system distance.
With J1023, where the distance was systematically underestimated, we hypothesised that an incorrect prior on the $A_V$ extinction was responsible.
However, we found consistent results when fitting with two possible $A_V$ priors.
Further to this, we ensured that the extinction was correctly calibrated to each band to avoid any doubt about our distance estimates

We performed our fitting of J1023 with an especially small band calibration uncertainty of 0.005 mag as we calibrated the photometry with extreme care.
This uncertainty accommodates for imprecisely calibrated magnitudes of each band of photometry by allowing for a small shift when fitting and can therefore introduce systematic errors into the distance estimates.
However, we found consistent results even with larger band calibration uncertainties, suggesting that the calibration is optimal.

We also considered the potential effect of a third light, which may act to push the distance further away.
However, this is not the only effect it would have on the system.
For J1023 the ellipsoidal modulation is the most significant, which is modelled by $i$, $q$ and $f$.
The inclination, $i$, affects the geometry of the system, such that a more edge on system will display great modulation amplitude.
Likewise, $q$ and $f$ define the size of the companion; a larger star will show ellipsoidal modulation of greater amplitude.
We derive $q$ from the companion radial velocity, $K_2$, which is well-constrained for J1023, leaving the free parameters $i$ and $f$.
When adding a third light to the system, any parameter changes can be absorbed by the inclination and filling factor, as well as the temperature.
For instance, the increased flux could be represented by a larger star (i.e. $f$ increases) if the irradiation temperature or inclination also adjust to keep the same ratio of irradiation to ellipsoidal modulation; adding a 3rd light dilutes the fractional variability of the total light.
This means that to reproduce the same total fractional variability, a model including a 3rd light would need the intrinsic companion-only LC to have a larger fraction ellipsoidal amplitude.
Thus, if $q$ is fixed and the distance does not change, this means $i$ moves closer to edge-on, or the filling factor increases.

In the end, this was roughly what we saw. 
By removing the expected flux from a third light, the filling factor decreased by around 5\%, suggesting that this change was absorbed by changing the size of the star.
 


\paragraph{Temperatures}
We have shown that the result of the fitting is highly model-dependent, and that there is no one-size-fits all solution at this stage.
Beyond just the goodness-of-fit, the choice of model can have a significant effect on the best-fit parameters, such as the pulsar mass estimates of J1227 or the filling factor and distance of J1023.
This is due to the considerable degeneracy between parameters in the \icarustt\ model; this degeneracy is normally broken by determining the temperature of the companion through modelling.
By introducing model extensions which alter the temperature distribution of the companion, either by adding flux in the form of a hot spot or by redistribution, we cause the degeneracy to be broken in a different way.
This also extends to the different heat redistribution models, such as whether the effects of diffusion are included along with convection.
It should be noted that these changes are small but significant.
However, it should be noted that the black-body temperatures of the day and night side of the companion are comparable between models despite often significant changes in individual temperature parameters.

For instance, for J1023 we obtain a significantly larger irradiation temperature for the convection and diffusion model than with the hot spot model; $\sim 7350$~K compared to $\sim 4700$~K.
With convection and diffusion, the heat is more efficiently redistributed over the star, lowering the actual day-side temperature.
Conversely, the spot model has the extra contribution from the hot spot on the day side of the star, reducing the required irradiation for the same modulation amplitude.
We see this with the broadly similar blackbody temperatures, however there are some consequences for the model.
The heat diffusion allows for a larger ellipsoidal modulation with a smaller filling factor, $f~\sim~0.81$ compared to $f~\sim~0.94$.
This is then compensated by bringing the system closer such that the correct flux is received.
Indeed, the heat redistribution model with diffusion and convection returned a significantly smaller distance than any other model, and certainly significantly below the prior value.
This may suggest that despite the physical motivation, the heat redistribution model is not an accurate description of the mechanism behind the asymmetry in this system.

We note a further consequence of this difference in irradiation temperature.
While the temperature distribution of the companion star may not be significantly changed in this case, a larger irradiation temperature implies that the companion intercepts more energy from the pulsar.
This could be caused by either a larger spin-down luminosity, $\dot{E}$, or an increase in the irradiation efficiency.
However, because of the model dependency of the irradiation temperature we do not claim that these changes are indeed observed.

\paragraph{Spot constraints}
Initially when implementing the hot spot model we made no assumptions about the parameters (spot temperature, $T_{\rm spot}$, spot radius, $R_{\rm spot}$, and polar coordinates $(\theta, \phi)$) other than physical constrains like $T_{\rm spot}/K \geq 0,\ R_{\rm spot}/{\rm deg} \geq 0$.
Our initial modelling revealed a strong preference for very small, very hot spots, with $T_{\rm spot} > 2000$ K and $R_{\rm spot} < 5$\textdegree.
We interpreted this as a sign of overfitting.
While we might expect star spots to be small compared to the stellar radius, combined with the very high temperature this suggests that this combination is not physical.
For instance, it is unlikely that the heat would not dissipate within the convective layer of the star and instead remain contained in a tight spot without a significant magnetic field. 
We note that \citet{Kandel2020} describe hot spots at the magnetic pole of the companion, though these spots have a radius of around $30-45$\textdegree, suggesting that even under the influence of magnetic fields the spots are not that small. 
However, we had no physical constraints to apply to these parameters.

As such, we introduced the spot intensity, $I_{\rm spot} = T_{\rm spot}^4R_{\rm spot}^2$ with units K$^4$~deg$^2$, as a further parameter which is analogous to the luminosity.
This parameter is constrained by a Gaussian prior centred around zero with a large ($\sim 10^{10}-10^{15}$) width; note that a change in this prior by a factor of $10^4$ results in a change in temperature by a factor of 10.
With this parameter, very hot spots (with necessarily small radii) are selected against.
Despite this, a small, hot spot for J1227 remains, with the best-fitting spot temperature of $2100$~K and radius of 7.8\textdegree\ with a spot prior width of $10^{12}$.
The reason for this is simply that the penalty for the spot is not sufficient to alter the fit, indicating a strong preference for this configuration.

This highlights one shortfall of modelling, in that the best fitting solution does not always correlate with physical expectations and priors.
This is also seen in the consistent underestimation of the system distance of J1023, despite strong priors, or the mass estimates of J1227.
%TODO: talk about covariances?


%- Fixing inclination 

%- Fixing distance

%- Constraining mass vs inclination


\subsection{GOTO}
\subsubsection{Lessons for surveys}
While the sky coverage, sensitivity, and resolution of GOTO mean it is excellently placed as a transient laboratory, the results discussed in chapter \ref{ch:goto} show that there is some way to go before this performance extends to wider uses.
We have seen that the photometry produced by GOTO is not sufficiently tailored for use in periodicity searching or classification of fainter objects, though \citet{Mong2020} present promising results on the classification of supernovae light curves.
Naturally, the primary focus of GOTO should remain as a transient laboratory for the detection of gravitational wave counterparts, but how could the program be modified so that we may also obtain photometry useful for our aims?

The importance of reliable photometry is also key for period searching and classification, that is, consistent magnitude measurements and robust detection of cosmic rays.
We found that, even after cleaning through two methods of clipping, the light curves displayed significant artefacts. %\tcr{(Figure of suspect light curve here or in GOTO chapter)}
These artefacts have a significant impact on both the periodicity searching and classification performance.
For the periodicity search they increase the $\chi^2$ statistic of harmonic series fits even at the true period, reducing the power of true peaks in the periodogram compared to the background.
For the classifier, the artefacts will remain after phase folding and so have an affect on the Fourier components when fit.
It is likely that this is a contributing factor to the inability of the classifier to distinguish between classes with similar periods, magnitudes, and $B_P - R_P$ colours.

The key finding of this research, however, is is how large an effect the observing cadence has on the performance of periodicity search algorithms.
We have shown that small changes to the observation schedule can result in a significant increase in periodicity search performance through the addition of random noise to observation times, without compromising the daily observations required of GOTO.
Furthermore, the periodicity search performance is an important precursor to the classifier performance, due to both the phase folding of light curves as well as the period estimate itself being the dominant feature.
Without concrete period estimates, we must rely on matching to existing surveys (such as ASAS-SN, as used in this research) for ephemerides. 
In this case, the fainter limiting of magnitude of ASAS-SN resulted in a significant fraction of GOTO light curves without matching ASAS-SN periods, nullifying the advantage of such a deep survey.
It is at this point that the importance of ancillary information becomes clear.



\subsubsection{The importance of ancillary information}
In this research, we make use of two ancillary data sources; the GAIA database and the ASAS-SN catalogue of variable stars.
The former provides the essential calibrated $G$-band magnitudes, distances, and $B_P - R_P$ colour information, while the latter provides periods, as well as our classifier training data set and labels for the testing data.
In our case, the significance of the $G$-band magnitude and $B_P - R_P$ magnitude is clear given the importance of these classification features.
Without these the classifier would be relying solely on the GOTO light curves and magnitudes, and the periods derived from these.
Given the results we have obtained, including the challenges faced searching for periods and the inconsistent magnitude calibration of the GOTO $L$-band observations, this cross matching is essential.
However, the process of cross-matching with other data sets can introduce a new set of problems, like the elimination of fainter GOTO sources without ASAS-SN matches.

A clear solution would be to use the multi-band imaging that GOTO is capable of.
Not only will this provide the needed colour information for precise classification of variables, but will also allow the use of multi-band light curves for period classification.
It is well-known that multi-band periodograms can outperform single-band periodograms \citep{VanderPlas2018}, especially when the signal is not strong in individual bands. 
Since GOTO is not capable of simultaneous multi-band imaging the same way that ULTRACAM is, for example, the necessary staggering of observations in each band may also help alleviate the problems with aliasing.



\subsubsection{Other surveys}
We will now discuss some of the approaches taken by contemporaries of GOTO; other all sky or transient surveys.
The most immediate comparison can be made with the BlackGEM project, as this telescope shares the aim of detecting gravitational wave counterpart transients.
With the first phase installed at the La Silla observatory, Chile, in 2019, BlackGEM consists of three wide-field telescopes with 0.65 m primary mirrors, individually mounted \citep{Bloemen2015}.
Each will have a field of view of 2.7 square degrees with a CCD resolution of 10k by 10k pixels for a pixel scale of 0.56 in/px \citep{Roelfsema2016} . 
The resolution is seeing limited at 1 arcsec, and the sensitivity is expected to extend to 23rd magnitude in SDSS g band with a 5-minute exposure \citep{Groot2019}.
Similar to GOTO, the primary function as a transient laboratory will be augmented by operating as a deep sky survey (though only in the southern  sky) in SDSS ugriz filters, on similar timescales to GOTO of hours to days.
The MeerLICHT telescope has identical specifications to BlackGEM, but instead shadows the MeerKAT radio telescope in Sutherland, South Africa \citep{Bloemen2016}. 

A precursor to these projects is the SkyMapper telescope, and the associated Southern Sky Survey \citep{Keller2007}.
SkyMapper is a single, larger, 1.3 m telescope with a 5.7 square degree field of view, with a similarly high CCD resolution of 16k by 16k pixels.
This results in a pixel scale of 0.5 arcsec such that the telescope is also seeing-limited.
This larger telescope is capable of photometry accuracy of better than 3\% down to 23rd magnitude in SDSS g bands.
Like BlackGEM it has interchangeable SDSS ugriz filters, with an additional v-band filter.
SkyMapper is optimised for a range of stellar astrophysics science goals, such that it used multi-epoch sampling; from 4 hours through 1 day, week, month, and year.
This range of epochs may help to alleviate aliasing issues if this survey is used for periodicity searching, though it is possible that the periodogram would be contaminated with even more aliased peaks.
\citet{Akhter2013} successfully recover 90\% known RR Lyrae variables in the globular cluster NGC 3201, though periodicity searching techniques were not used, instead by calculating the variability amplitude and colour information to identify RR Lyraes.
However, this illustrates the sensitivity possible with SkyMapper.

\citet{Andreoni2020} present results from the Dark Energy Survey's Dark Energy Camera (DECam), which used continuous 20 s exposures with a magnitude limit of $g > 23$.
This instrument is considerably more specialised than the others discussed here; the project does not aim for full sky coverage, and the extremely short cadence is more suited to fast transient detection than detection of most variable stars.

The Vera Rubin Observatory (shorthand `Rubin', formerly the Large Synoptic Survey Telescope), conducting the Legacy Survey of Space and Time (LSST) has perhaps the widest range of science goals yet; from mapping objects in the solar system, through high redshift galaxies and supernovae, to the distribution of galaxies and cosmological constraints. 
This large telescope has an effective aperture of 6.7 meters and a field of view of 9.6 square degrees with a total survey area of over 20,000 square degrees.
Each pointing will be imaged 2000 times over the course of the 10 year program, with fifteen second exposures in six broad bands modelled on the SDSS ugriz(y) system, to a maximum depth of $r\sim~27.5$ \citep{LSST2009}.
From the real-time classification pipeline of \citet{Narayan2018}, to promising recovery of simulated RR Lyrae light curve periods \citep{Oluseyi2012}, the breadth of this project and the volume of data have already produced interesting research which highlights the potential of such a system for the detection and classification of periodic variables.
Though naturally, the size (and indeed budget) of the LSST or Rubin cannot be fairly compared to GOTO.

Lastly, the Zwicky Transient Facility (ZTF), the successor of the Palomar Transient Factory (PTF).
The ZTF uses a 47 square degree wide-field camera on the Palomar 48 inch Schmidt telescope, representing more than an order of magnitude improvement in survey speed over the PTF \citep{Bellm2018}. 
It uses $g$, $r$, and $i$-band filters.
The ZTF aims to discover new optical transients and variables through all-sky surveys, and as a result uses relative photometry as well as point source function (PSF) photometry for more accurate light curves.\\

While the science aims are often different, from the gravitational wave transient detection of BlackGEM and GOTO to the broad goals of the LSST, one of the key similarities between these surveys is the use of multi-band imaging.
The advantages for classification of detections is clear, as colour information and its variation over time is one of the most important measures in optical astronomy.
Another facet of surveys like the ZTF is the application of relative photometry correction (see \citet{Masci2019a}, ensuring more consistent calibration of light curves, which means identification of these light curves is more reliable.

GOTO sits among the smallest telescopes, smaller even than BlackGEM, however the field of view is one of the largest.
The smaller collecting area means that GOTO is not as sensitive as its contemporaries; considering the faintness of black widow pulsars, especially at their optical minima, a limiting magnitude of $\sim20.5$ is not sufficient for reliable detections.
In this regard, deeper surveys like the LSST may be better-suited for discovering the optical counterparts to spider pulsars.

Surveys like the ZTF and the LSST show that a primary aim of transient detection can be performed alongside standard photometry.
While each takes a different approach, this shows that small adjustments to the GOTO program will provide bountiful returns for periodicity searching, classification, and other research with well-calibrated photometry.


\section{Conclusions}
The work in this thesis concerns two extremes of photometric data, from the precisely-calibrated, high time resolution photometry from ULTRACAM to the sparse, noisy data from GOTO.
However, in both cases the work revealed significant intricacies.

For the modelling of ULTRACAM photometry in chapter \ref{ch:tmsps} we see that the modelling was unable to fully account for the asymmetric shape of the light curve.
While this asymmetry provides a substantial challenge for modelling, it also provides a window to new physics.
With lower-quality data it is likely that some of this fine structure would be obscured by noise, but in this case it reveals that our current models are not a complete description of the system.
However, we were able to obtain coarse estimates of the mass and inclination of both systems, as well as finding a system temperature that is consistent with other measurements.
Notably, we find slightly under-filled Roche lobes in both systems, which may give insights into the nature of the transition mechanism; we expect the Roche lobes of tMSPs to be over-full in the AP state, suggesting that the star shrinks during the transition to the RP state.
While these results are not as complete as one may hope, they encourage further study of these, and other, tMSP systems to further unveil their role in the evolution of spider pulsars.

While the periodicity search pipeline and ML classifier discussed in chapters \ref{ch:observations} and \ref{ch:goto} did not produce satisfactory results when implemented on GOTO data, they did highlight several key shortcomings of this photometry.
This work is instead useful as a means of illustrating the desirable properties of photometric surveys - namely well-calibrated photometry, multi-band imaging, low aliasing, and good phase coverage - and showing the limitations which occur when some or all of these are missing.
The importance of ancillary information has also been demonstrated.
It should be noted that this work is focused on the study of short-period binaries, which are generally fainter than other transients like supernovae. 
Indeed, \citet{Mong2020} and \citet{Killestein2021} have shown very promising results in the detection and classification of brighter transients.
However, if we wish to expand these successes to the field of spider pulsars, then adjustments must be made in the scheduling of observations to reduce aliasing and additional colour information must be available.
As well as this, we have shown that the quality of photometry must be improved when studying these faint sources, as the shape of the optical light curve is a key feature for differentiating between otherwise similar variables.

To summarise, this work typifies some of the key issues in modern astronomy; that the quality of the model and the data are rarely in tandem.
However, we have shown that with some perseverance, interesting results can be obtained and where this is not possible provided a discussion on how best to proceed for future investigations.